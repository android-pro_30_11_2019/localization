package com.example.localization;


import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.akexorcist.localizationactivity.core.LocalizationActivityDelegate;
import com.akexorcist.localizationactivity.core.OnLocaleChangedListener;

import java.util.Locale;

public abstract class BaseActivity extends AppCompatActivity implements OnLocaleChangedListener {

    private LocalizationActivityDelegate localizationDelegate = new LocalizationActivityDelegate(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        localizationDelegate.addOnLocaleChangedListener(this);
        localizationDelegate.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        localizationDelegate.onResume(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(newBase));
    }

    @Override
    public void onAfterLocaleChanged() {

    }

    @Override
    public void onBeforeLocaleChanged() {

    }

    @Override
    public Context getApplicationContext() {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }

    @Override
    public Resources getResources() {
        return localizationDelegate.getResources(super.getResources());
    }
    public void setLanguage(String language){
        localizationDelegate.setLanguage(this, language);
    }
    public void setLanguage(String language, String country){
        localizationDelegate.setLanguage(this,language,country);
    }
    public void setLanguage(Locale locale){
        localizationDelegate.setLanguage(this,locale);
    }
    public Locale getCurrentLanguage(){
        return localizationDelegate.getLanguage(this);
    }
    public final Locale getLocale() {
        return localizationDelegate.getLanguage(getApplicationContext());
    }
    public final void setDefaultLanguage(Locale language) {
        localizationDelegate.setDefaultLanguage(language);
    }
}
