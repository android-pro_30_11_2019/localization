package com.example.localization;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import java.util.Locale;

public class MainActivity extends BaseActivity {
    private Button btnChangefr,btnChangezh;
    private TextView tvHello;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setDefaultLanguage(Locale.getDefault());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnChangefr = findViewById(R.id.btnChangefr);
        btnChangezh = findViewById(R.id.btnChangezh);

        tvHello = findViewById(R.id.tvHello);

        btnChangefr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLanguage(Locale.FRANCE);
                Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            }
        });
        btnChangezh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLanguage(Locale.SIMPLIFIED_CHINESE);
            }
        });


    }
}
